/*IMPORTANT: This is where important information pertaining
to the currenct version is.

At the moment, a move consists of four characters, consisting
of the current position followed by the final position of the
piece. Algebraic notation will implemented later.
*/

#include <iostream>
#include <vector>
#include <cmath>
#include "chess.h"

using namespace std;

vector<int> getCos(string pos);
vector<int> algebraicConv(string move, string colour, Square board[][8]);
void printBoard(Square board[][8]);
void makeMove(Square board[][8], int fromX, int fromY, int toX, int toY, string colour, int *KingX, int *KingY, bool &made);
//VALIDITY CHECKS, complete, including castling -- still need to do en passe.
bool Piece::whiteCheckValid(int fromX, int fromY, int toX, int toY, Square board[][8]){
        //PAWN
        if(toX == fromX && toY == fromY){
                return false;
        }
        if(board[toX][toY].getPiece().getColour() == "white"){
                return false;
        }
        if(type == "pawn"){
                if((toY - fromY) == 1 && toX==fromX){
                        return true;
                }else if((toY - fromY) == 2 && toX==fromX){
                        if(first && board[fromX][fromY + 1].getPiece().getColour() == "clear" && board[toX][toY].getPiece().getColour() == "clear"){
                                return true;
                        }else{
                                return false;
                        }
                }else if((toY - fromY) == 1 && abs(toX - fromX) == 1){
                        if(board[toX][toY].getPiece().getColour() == "black"){
                                return true;
                        }
                }
                return false;
        }else if(type == "knight"){
                //KNIGHT
                if(abs(toX - fromX) == 2 && abs(toY - fromY)==1){
                        return true;
                }else if(abs(toX - fromX) == 1 && abs(toY - fromY)==2){
                        return true;
                }else{
                        return false;
                }
        }else if(type == "rook"){
                //ROOK
                if(toX == fromX){
                        int diff = toY - fromY;
                        int inc;
                        if(diff < 0){
                                inc = -1;
                        }else{
                                inc = 1;
                        }
                        for(int i = 1; i<abs(diff); i++){
                                if(board[toX][fromY + i*inc].getPiece().getColour() != "clear"){
                                        return false;
                                }
                        }
                        return true;
                }else if(toY == fromY){
                        int diff = toX - fromX;
                        int inc;
                        if(diff < 0){
                                inc = -1;
                        }else{
                                inc = 1;
                        }
                        for(int i = 1; i<abs(diff); i++){
                                if(board[fromX + i*inc][toY].getPiece().getColour() != "clear"){
                                        return false;
                                }
                        }
                        return true;
                }else{
                        return false;
                }
        }else if(type == "bishop"){
                //BISHOP
                if(abs(toX - fromX)==abs(toY - fromY)){
                        int xInc;
                        int yInc;
                        int xDiff = toX - fromX;
                        int yDiff = toY - fromY;
                        if(xDiff < 0){
                                xInc = -1;
                        }else{
                                xInc = 1;
                        }
                        if(yDiff < 0){
                                yInc = -1;
                        }else{
                                yInc = 1;
                        }
                        int diff = abs(xDiff);
                        for(int i = 1; i<diff; i++){
                                if(board[fromX + i*xInc][fromY+i*yInc].getPiece().getColour() != "clear"){
                                        return false;
                                }
                        }
                        return true;
                }else{
                        return false;
                }
        }else if(type == "queen"){
                //QUEEN
                if(toX == fromX){
                        int diff = toY - fromY;
                        int inc;
                        if(diff < 0){
                                inc = -1;
                        }else{
                                inc = 1;
                        }
                        for(int i = 1; i<abs(diff); i++){
                                if(board[toX][fromY + i*inc].getPiece().getColour() != "clear"){
                                        return false;
                                }
                        }
                        return true;
                }else if(toY == fromY){
                        int diff = toX - fromX;
                        int inc;
                        if(diff < 0){
                                inc = -1;
                        }else{
                                inc = 1;
                        }
                        for(int i = 1; i<abs(diff); i++){
                                if(board[fromX + i*inc][toY].getPiece().getColour() != "clear"){
                                        return false;
                                }
                        }
                        return true;
                }else if(abs(toX - fromX)==abs(toY - fromY)){
                        int xInc;
                        int yInc;
                        int xDiff = toX - fromX;
                        int yDiff = toY - fromY;
                        if(xDiff < 0){
                                xInc = -1;
                        }else{
                                xInc = 1;
                        }
                        if(yDiff < 0){
                                yInc = -1;
                        }else{
                                yInc = 1;
                        }
                        int diff = abs(xDiff);
                        for(int i = 1; i<diff; i++){
                                if(board[fromX + i*xInc][fromY+i*yInc].getPiece().getColour() != "clear"){
                                        return false;
                                }
                        }
                        return true;
                }else{
                        return false;
                }
        }else if(type == "king"){
                //KING
                if(abs(toX - fromX)<2 && abs(toY - fromY)<2){
                        return true;
                }else if(toX - fromX == 2 && toY == fromY){
                        //castle option
                        if(board[fromX][fromY].getPiece().firstMove()){
                                        if(toX - fromX < 0){
                                                //castling left
                                                if(board[0][0].getPiece().firstMove() && board[0][0].getPiece().getType() == "rook"){
                                                        if(board[1][0].getPiece().getColour() == "clear"
                                                         && board[2][0].getPiece().getColour() == "clear"
                                                         && board[3][0].getPiece().getColour() == "clear"){
                                                                 board[3][0].setPiece(board[0][0].getPiece());
                                                                 board[0][0].removePiece();
                                                                 return true;
                                                         }else{
                                                                 return false;
                                                         }
                                                }else{
                                                        return false;
                                                }
                                        }else{
                                                //castling right
                                                if(board[7][0].getPiece().firstMove() && board[7][0].getPiece().getType() == "rook"){
                                                        if(board[6][0].getPiece().getColour() == "clear"
                                                         && board[5][0].getPiece().getColour() == "clear"){
                                                                 board[5][0].setPiece(board[7][0].getPiece());
                                                                 board[7][0].removePiece();
                                                                 return true;
                                                         }else{
                                                                 return false;
                                                         }
                                                }else{
                                                        return false;
                                                }

                                        }
                        }else{
                                return false;
                        }
                }else{
                        return false;
                }
        }

        return true;
};
bool Piece::blackCheckValid(int fromX, int fromY, int toX, int toY, Square board[][8]){
        //PAWN
        if(toX == fromX && toY == fromY){
                return false;
        }
        if(board[toX][toY].getPiece().getColour() == "black"){
                return false;
        }
        if(type == "pawn"){
                if((toY - fromY) == -1 && toX==fromX){
                        return true;
                }else if((toY - fromY) == -2 && toX==fromX){
                        if(first && board[fromX][fromY - 1].getPiece().getColour() == "clear" && board[toX][toY].getPiece().getColour() == "clear"){
                                return true;
                        }else{
                                return false;
                        }
                }else if((toY - fromY) == -1 && abs(toX - fromX) == 1){
                        if(board[toX][toY].getPiece().getColour() == "white"){
                                return true;
                        }
                }
                return false;
        }else if(type == "knight"){
                if(abs(toX - fromX) == 2 && abs(toY - fromY)==1){
                        return true;
                }else if(abs(toX - fromX) == 1 && abs(toY - fromY)==2){
                        return true;
                }else{
                        return false;
                }
        }else if(type == "rook"){
                //ROOK
                if(toX == fromX){
                        int diff = toY - fromY;
                        int inc;
                        if(diff < 0){
                                inc = -1;
                        }else{
                                inc = 1;
                        }
                        for(int i = 1; i<abs(diff); i++){
                                if(board[toX][fromY + i*inc].getPiece().getColour() != "clear"){
                                        return false;
                                }
                        }
                        return true;
                }else if(toY == fromY){
                        int diff = toX - fromX;
                        int inc;
                        if(diff < 0){
                                inc = -1;
                        }else{
                                inc = 1;
                        }
                        for(int i = 1; i<abs(diff); i++){
                                if(board[fromX + i*inc][toY].getPiece().getColour() != "clear"){
                                        return false;
                                }
                        }
                        return true;
                }else{
                        return false;
                }
        }else if(type == "bishop"){
                //BISHOP
                if(abs(toX - fromX)==abs(toY - fromY)){
                        int xInc;
                        int yInc;
                        int xDiff = toX - fromX;
                        int yDiff = toY - fromY;
                        if(xDiff < 0){
                                xInc = -1;
                        }else{
                                xInc = 1;
                        }
                        if(yDiff < 0){
                                yInc = -1;
                        }else{
                                yInc = 1;
                        }
                        int diff = abs(xDiff);
                        for(int i = 1; i<diff; i++){
                                if(board[fromX + i*xInc][fromY+i*yInc].getPiece().getColour() != "clear"){
                                        return false;
                                }
                        }
                        return true;
                }else{
                        return false;
                }
        }else if(type == "queen"){
                //QUEEN
                if(toX == fromX){
                        int diff = toY - fromY;
                        int inc;
                        if(diff < 0){
                                inc = -1;
                        }else{
                                inc = 1;
                        }
                        for(int i = 1; i<abs(diff); i++){
                                if(board[toX][fromY + i*inc].getPiece().getColour() != "clear"){
                                        return false;
                                }
                        }
                        return true;
                }else if(toY == fromY){
                        int diff = toX - fromX;
                        int inc;
                        if(diff < 0){
                                inc = -1;
                        }else{
                                inc = 1;
                        }
                        for(int i = 1; i<abs(diff); i++){
                                if(board[fromX + i*inc][toY].getPiece().getColour() != "clear"){
                                        return false;
                                }
                        }
                        return true;
                }else if(abs(toX - fromX)==abs(toY - fromY)){
                        int xInc;
                        int yInc;
                        int xDiff = toX - fromX;
                        int yDiff = toY - fromY;
                        if(xDiff < 0){
                                xInc = -1;
                        }else{
                                xInc = 1;
                        }
                        if(yDiff < 0){
                                yInc = -1;
                        }else{
                                yInc = 1;
                        }
                        int diff = abs(xDiff);
                        for(int i = 1; i<diff; i++){
                                if(board[fromX + i*xInc][fromY+i*yInc].getPiece().getColour() != "clear"){
                                        return false;
                                }
                        }
                        return true;
                }else{
                        return false;
                }
        }else if(type == "king"){
                //KING
                if(abs(toX - fromX)<2 && abs(toY - fromY)<2){
                        return true;
                }else if(toX - fromX == 2 && toY == fromY){
                        //castle option
                        if(board[fromX][fromY].getPiece().firstMove()){
                                        if(toX - fromX < 0){
                                                //castling left
                                                if(board[0][7].getPiece().firstMove() && board[0][7].getPiece().getType() == "rook"){
                                                        if(board[1][7].getPiece().getColour() == "clear"
                                                         && board[2][7].getPiece().getColour() == "clear"
                                                         && board[3][7].getPiece().getColour() == "clear"){
                                                                 board[3][7].setPiece(board[0][7].getPiece());
                                                                 board[0][7].removePiece();
                                                                 return true;
                                                         }else{
                                                                 return false;
                                                         }
                                                }else{
                                                        return false;
                                                }
                                        }else{
                                                //castling right
                                                if(board[7][7].getPiece().firstMove() && board[7][7].getPiece().getType() == "rook"){
                                                        if(board[6][7].getPiece().getColour() == "clear"
                                                         && board[5][7].getPiece().getColour() == "clear"){
                                                                 board[5][7].setPiece(board[0][7].getPiece());
                                                                 board[0][7].removePiece();
                                                                 return true;
                                                         }else{
                                                                 return false;
                                                         }
                                                }else{
                                                        return false;
                                                }

                                        }
                        }else{
                                return false;
                        }
                }else{
                        return false;
                }
        }

        return true;
};
bool isAttackedByBlack(int X, int Y, Square board[][8]){
        //returns true if the square at (X, Y) is attacked by black.
        //assume there is no attack and try to find one.
        //bwe must make sure that we are accessing a square that exists at all times.
        //check for pawns:
                if(Y+1 < 8){
                        if(0 <= X-1){
                                if(board[X-1][Y+1].getPiece().getIdentifier() == "bpa"){
                                        return true;
                                }
                        }
                        if(X+1 < 8){
                                if(board[X+1][Y+1].getPiece().getIdentifier() == "bpa"){
                                        return true;
                                }
                        }
                }
        /*else{
                //then the colour is white, white pawns attack from below
                if(0<= Y-1){
                        if(0 <= X-1){
                                if(board[X-1][Y-1].getPiece().getIdentifier() == "bpa"){
                                        return true;
                                }
                        }
                        if(X+1 < 8){
                                if(board[X+1][Y-1].getPiece().getIdentifier() == "bpa"){
                                        return true;
                                }
                        }
                }
        }*/

        //check for KNIGHTS
        if(Y+2 < 8){
                if(X+1 < 8){
                        if(board[X+1][Y+2].getPiece().getIdentifier() == "bkn"){
                                return true;
                        }
                }
                if(0 <= X-1){
                        if(board[X-1][Y+2].getPiece().getIdentifier() == "bkn"){
                                return true;
                        }
                }
        }
        if(0 <= Y-2){
                if(X+1 < 8){
                        if(board[X+1][Y-2].getPiece().getIdentifier() == "bkn"){
                                return true;
                        }
                }
                if(0 <= X-1){
                        if(board[X-1][Y-2].getPiece().getIdentifier() == "bkn"){
                                return true;
                        }
                }
        }
        if(X+2 < 8){
                if(Y+1 < 8){
                        if(board[X+2][Y+1].getPiece().getIdentifier() == "bkn"){
                                return true;
                        }
                }
                if(0 <= Y-1){
                        if(board[X+2][Y-1].getPiece().getIdentifier() == "bkn"){
                                return true;
                        }
                }
        }
        if(0 <= X-2){
                if(Y+1 < 8){
                        if(board[X-2][Y+1].getPiece().getIdentifier() == "bkn"){
                                return true;
                        }
                }
                if(0 <= Y-1){
                        if(board[X-2][Y-1].getPiece().getIdentifier() == "bkn"){
                                return true;
                        }
                }
        }
        //finished checking for knights
        //checking for ROOKS and QUEENS
        bool found;
        //X DIRECTION
        found = false;
        for(int i = 0; i<X; i++){
                if(board[i][Y].getPiece().getIdentifier() == "bro" || board[i][Y].getPiece().getIdentifier() == "bqu"){
                        found = true;
                }
                if(board[i][Y].getPiece().getColour() == "white" ||
                board[i][Y].getPiece().getIdentifier() == "bpa" ||
                board[i][Y].getPiece().getIdentifier() == "bbi" ||
                board[i][Y].getPiece().getIdentifier() == "bkn" ||
                board[i][Y].getPiece().getIdentifier() == "bki"){
                        found = false;
                }
        }
        if(found){
                return true;
        }

        found = false;
        for(int i = 7; i>X; i--){
                if(board[i][Y].getPiece().getIdentifier() == "bro" || board[i][Y].getPiece().getIdentifier() == "bqu"){
                        found = true;
                }
                if(board[i][Y].getPiece().getColour() == "white" ||
                board[i][Y].getPiece().getIdentifier() == "bpa" ||
                board[i][Y].getPiece().getIdentifier() == "bbi" ||
                board[i][Y].getPiece().getIdentifier() == "bkn" ||
                board[i][Y].getPiece().getIdentifier() == "bki"){
                        found = false;
                }
        }
        if(found){
                return true;
        }
        //Y DIRECTION
        found = false;
        for(int i = 7; i>Y; i--){
                if(board[X][i].getPiece().getIdentifier() == "bro" || board[X][i].getPiece().getIdentifier() == "bqu"){
                        found = true;
                }
                if(board[X][i].getPiece().getColour() == "white" ||
                board[X][i].getPiece().getIdentifier() == "bpa" ||
                board[X][i].getPiece().getIdentifier() == "bbi" ||
                board[X][i].getPiece().getIdentifier() == "bkn" ||
                board[X][i].getPiece().getIdentifier() == "bki"){
                        found = false;
                }
        }
        if(found){
                return true;
        }

        found = false;
        for(int i = 0; i<Y; i++){
                if(board[X][i].getPiece().getIdentifier() == "bro" || board[X][i].getPiece().getIdentifier() == "bqu"){
                        found = true;
                }
                if(board[X][i].getPiece().getColour() == "white" ||
                board[X][i].getPiece().getIdentifier() == "bpa" ||
                board[X][i].getPiece().getIdentifier() == "bbi" ||
                board[X][i].getPiece().getIdentifier() == "bkn" ||
                board[X][i].getPiece().getIdentifier() == "bki"){
                        found = false;
                }
        }
        if(found){
                return true;
        }
        //rooks and half of queen done
        //BISHOPS
        for(int i = 1; X+i < 8 && Y+i < 8; i++){
                if(board[X+i][Y+i].getPiece().getColour() == "white" ||
                board[X+i][Y+i].getPiece().getIdentifier() == "bpa" ||
                board[X+i][Y+i].getPiece().getIdentifier() == "bro" ||
                board[X+i][Y+i].getPiece().getIdentifier() == "bkn" ||
                board[X+i][Y+i].getPiece().getIdentifier() == "bki"){
                        break;
                }
                if(board[X+i][Y+i].getPiece().getIdentifier() == "bbi" || board[X+i][Y+i].getPiece().getIdentifier() == "bqu"){
                        return true;
                }
        }
        for(int i = 1; 0 <= X-i && Y+i < 8; i++){
                if(board[X-i][Y+i].getPiece().getColour() == "white" ||
                board[X-i][Y+i].getPiece().getIdentifier() == "bpa" ||
                board[X-i][Y+i].getPiece().getIdentifier() == "bro" ||
                board[X-i][Y+i].getPiece().getIdentifier() == "bkn" ||
                board[X-i][Y+i].getPiece().getIdentifier() == "bki"){
                        break;
                }
                if(board[X-i][Y+i].getPiece().getIdentifier() == "bbi" || board[X-i][Y+i].getPiece().getIdentifier() == "bqu"){
                        return true;
                }
        }
        for(int i = 1; X+i < 8 && 0 <= Y-i; i++){
                if(board[X+i][Y-i].getPiece().getColour() == "white" ||
                board[X+i][Y-i].getPiece().getIdentifier() == "bpa" ||
                board[X+i][Y-i].getPiece().getIdentifier() == "bro" ||
                board[X+i][Y-i].getPiece().getIdentifier() == "bkn" ||
                board[X+i][Y-i].getPiece().getIdentifier() == "bki"){
                        break;
                }
                if(board[X+i][Y-i].getPiece().getIdentifier() == "bbi" || board[X+i][Y-i].getPiece().getIdentifier() == "bqu"){
                        return true;
                }
        }
        for(int i = 1; 0 <= X-i && 0 <= Y-i;i++){
                if(board[X-i][Y-i].getPiece().getColour() == "white" ||
                board[X-i][Y-i].getPiece().getIdentifier() == "bpa" ||
                board[X-i][Y-i].getPiece().getIdentifier() == "bro" ||
                board[X-i][Y-i].getPiece().getIdentifier() == "bkn" ||
                board[X-i][Y-i].getPiece().getIdentifier() == "bki"){
                        break;
                }
                if(board[X-i][Y-i].getPiece().getIdentifier() == "bbi" || board[X-i][Y-i].getPiece().getIdentifier() == "bqu"){
                        return true;
                }
        }
        //now have checked for all attacks

        return false; //no attacking positions found
}
bool isAttackedByWhite(int X, int Y, Square board[][8]){
        //returns true if the square at (X, Y) is attacked by black.
        //assume there is no attack and try to find one.
        //bwe must make sure that we are accessing a square that exists at all times.
        //check for pawns:

                //then the colour is white, white pawns attack from below
                if(0<= Y-1){
                        if(0 <= X-1){
                                if(board[X-1][Y-1].getPiece().getIdentifier() == "wpa"){
                                        return true;
                                }
                        }
                        if(X+1 < 8){
                                if(board[X+1][Y-1].getPiece().getIdentifier() == "wpa"){
                                        return true;
                                }
                        }
                }
        //check for KNIGHTS
        if(Y+2 < 8){
                if(X+1 < 8){
                        if(board[X+1][Y+2].getPiece().getIdentifier() == "wkn"){
                                return true;
                        }
                }
                if(0 <= X-1){
                        if(board[X-1][Y+2].getPiece().getIdentifier() == "wkn"){
                                return true;
                        }
                }
        }
        if(0 <= Y-2){
                if(X+1 < 8){
                        if(board[X+1][Y-2].getPiece().getIdentifier() == "wkn"){
                                return true;
                        }
                }
                if(0 <= X-1){
                        if(board[X-1][Y-2].getPiece().getIdentifier() == "wkn"){
                                return true;
                        }
                }
        }
        if(X+2 < 8){
                if(Y+1 < 8){
                        if(board[X+2][Y+1].getPiece().getIdentifier() == "wkn"){
                                return true;
                        }
                }
                if(0 <= Y-1){
                        if(board[X+2][Y-1].getPiece().getIdentifier() == "wkn"){
                                return true;
                        }
                }
        }
        if(0 <= X-2){
                if(Y+1 < 8){
                        if(board[X-2][Y+1].getPiece().getIdentifier() == "wkn"){
                                return true;
                        }
                }
                if(0 <= Y-1){
                        if(board[X-2][Y-1].getPiece().getIdentifier() == "wkn"){
                                return true;
                        }
                }
        }
        //finished checking for knights
        //checking for ROOKS and QUEENS
        bool found;
        //X DIRECTION
        found = false;
        for(int i = 0; i<X; i++){
                if(board[i][Y].getPiece().getIdentifier() == "wro" || board[i][Y].getPiece().getIdentifier() == "wqu"){
                        found = true;
                }
                if(board[i][Y].getPiece().getColour() == "black" ||
                board[i][Y].getPiece().getIdentifier() == "wpa" ||
                board[i][Y].getPiece().getIdentifier() == "wbi" ||
                board[i][Y].getPiece().getIdentifier() == "wkn" ||
                board[i][Y].getPiece().getIdentifier() == "wki"){
                        found = false;
                }
        }
        if(found){
                return true;
        }

        found = false;
        for(int i = 7; i>X; i--){
                if(board[i][Y].getPiece().getIdentifier() == "wro" || board[i][Y].getPiece().getIdentifier() == "wqu"){
                        found = true;
                }
                if(board[i][Y].getPiece().getColour() == "white" ||
                board[i][Y].getPiece().getIdentifier() == "wpa" ||
                board[i][Y].getPiece().getIdentifier() == "wbi" ||
                board[i][Y].getPiece().getIdentifier() == "wkn" ||
                board[i][Y].getPiece().getIdentifier() == "wki"){
                        found = false;
                }
        }
        if(found){
                return true;
        }
        //Y DIRECTION
        found = false;
        for(int i = 7; i>Y; i--){
                if(board[X][i].getPiece().getIdentifier() == "wro" || board[X][i].getPiece().getIdentifier() == "wqu"){
                        found = true;
                }
                if(board[X][i].getPiece().getColour() == "black" ||
                board[X][i].getPiece().getIdentifier() == "wpa" ||
                board[X][i].getPiece().getIdentifier() == "wbi" ||
                board[X][i].getPiece().getIdentifier() == "wkn" ||
                board[X][i].getPiece().getIdentifier() == "wki"){
                        found = false;
                }
        }
        if(found){
                return true;
        }

        found = false;
        for(int i = 0; i<Y; i++){
                if(board[X][i].getPiece().getIdentifier() == "wro" || board[X][i].getPiece().getIdentifier() == "wqu"){
                        found = true;
                }
                if(board[X][i].getPiece().getColour() == "black" ||
                board[X][i].getPiece().getIdentifier() == "wpa" ||
                board[X][i].getPiece().getIdentifier() == "wbi" ||
                board[X][i].getPiece().getIdentifier() == "wkn" ||
                board[X][i].getPiece().getIdentifier() == "wki"){
                        found = false;
                }
        }
        if(found){
                return true;
        }
        //rooks and half of queen done
        //BISHOPS
        for(int i = 1; X+i < 8 && Y+i < 8; i++){
                if(board[X+i][Y+i].getPiece().getColour() == "black" ||
                board[X+i][Y+i].getPiece().getIdentifier() == "wpa" ||
                board[X+i][Y+i].getPiece().getIdentifier() == "wro" ||
                board[X+i][Y+i].getPiece().getIdentifier() == "wkn" ||
                board[X+i][Y+i].getPiece().getIdentifier() == "wki"){
                        break;
                }
                if(board[X+i][Y+i].getPiece().getIdentifier() == "wbi" || board[X+i][Y+i].getPiece().getIdentifier() == "wqu"){
                        return true;
                }
        }
        for(int i = 1; 0 <= X-i && Y+i < 8; i++){
                if(board[X-i][Y+i].getPiece().getColour() == "black" ||
                board[X-i][Y+i].getPiece().getIdentifier() == "wpa" ||
                board[-i][Y+i].getPiece().getIdentifier() == "wro" ||
                board[X-i][Y+i].getPiece().getIdentifier() == "wkn" ||
                board[X-i][Y+i].getPiece().getIdentifier() == "wki"){
                        break;
                }
                if(board[X-i][Y+i].getPiece().getIdentifier() == "wbi" || board[X-i][Y+i].getPiece().getIdentifier() == "wqu"){
                        return true;
                }
        }
        for(int i = 1; X+i < 8 && 0 <= Y-i; i++){
                if(board[X+i][Y-i].getPiece().getColour() == "black" ||
                board[X+i][Y-i].getPiece().getIdentifier() == "wpa" ||
                board[X+i][Y-i].getPiece().getIdentifier() == "wro" ||
                board[X+i][Y-i].getPiece().getIdentifier() == "wkn" ||
                board[X+i][Y-i].getPiece().getIdentifier() == "wki"){
                        break;
                }
                if(board[X+i][Y-i].getPiece().getIdentifier() == "wbi" || board[X+i][Y-i].getPiece().getIdentifier() == "wqu"){
                        return true;
                }
        }
        for(int i = 1; 0 <= X-i && 0 <= Y-i;i++){
                if(board[X-i][Y-i].getPiece().getColour() == "black" ||
                board[X-i][Y-i].getPiece().getIdentifier() == "wpa" ||
                board[X-i][Y-i].getPiece().getIdentifier() == "wro" ||
                board[X-i][Y-i].getPiece().getIdentifier() == "wkn" ||
                board[X-i][Y-i].getPiece().getIdentifier() == "wki"){
                        break;
                }
                if(board[X-i][Y-i].getPiece().getIdentifier() == "wbi" || board[X-i][Y-i].getPiece().getIdentifier() == "wqu"){
                        return true;
                }
        }
        //now have checked for all attacks

        return false; //no attacking positions found
}

int main(){
        //INITIALISE board
        Square board[8][8];
        //Initialise pieces
        Piece wRook1 = Piece("white", "rook");
        Piece wRook2 = Piece("white", "rook");
        Piece bRook1 = Piece("black", "rook");
        Piece bRook2 = Piece("black", "rook");
        Piece wKnight1 = Piece("white", "knight");
        Piece wKnight2 = Piece("white", "knight");
        Piece bKnight1 = Piece("black", "knight");
        Piece bKnight2 = Piece("black", "knight");
        Piece wBishop1 = Piece("white", "bishop");
        Piece wBishop2 = Piece("white", "bishop");
        Piece bBishop1 = Piece("black", "bishop");
        Piece bBishop2 = Piece("black", "bishop");
        Piece wQueen = Piece("white", "queen");
        Piece bQueen = Piece("black", "queen");
        Piece wKing = Piece("white", "king");
        Piece bKing = Piece("black", "king");
        Piece wPawn1 = Piece("white", "pawn");
        Piece wPawn2 = Piece("white", "pawn");
        Piece wPawn3 = Piece("white", "pawn");
        Piece wPawn4 = Piece("white", "pawn");
        Piece wPawn5 = Piece("white", "pawn");
        Piece wPawn6 = Piece("white", "pawn");
        Piece wPawn7 = Piece("white", "pawn");
        Piece wPawn8 = Piece("white", "pawn");
        Piece bPawn1 = Piece("black", "pawn");
        Piece bPawn2 = Piece("black", "pawn");
        Piece bPawn3 = Piece("black", "pawn");
        Piece bPawn4 = Piece("black", "pawn");
        Piece bPawn5 = Piece("black", "pawn");
        Piece bPawn6 = Piece("black", "pawn");
        Piece bPawn7 = Piece("black", "pawn");
        Piece bPawn8 = Piece("black", "pawn");
        //for testing Piece dummyPawn = Piece("black", "pawn");
        //place pieces onto board
        //white
        board[0][0].setPiece(wRook1);
        board[1][0].setPiece(wKnight1);
        board[2][0].setPiece(wBishop1);
        board[3][0].setPiece(wQueen);
        board[4][0].setPiece(wKing);
        board[5][0].setPiece(wBishop2);
        board[6][0].setPiece(wKnight2);
        board[7][0].setPiece(wRook2);
        board[0][1].setPiece(wPawn1);
        board[1][1].setPiece(wPawn2);
        board[2][1].setPiece(wPawn3);
        board[3][1].setPiece(wPawn4);
        board[4][1].setPiece(wPawn5);
        board[5][1].setPiece(wPawn6);
        board[6][1].setPiece(wPawn7);
        board[7][1].setPiece(wPawn8);
        //black
        board[0][7].setPiece(bRook1);
        board[1][7].setPiece(bKnight1);
        board[2][7].setPiece(bBishop1);
        board[3][7].setPiece(bQueen);
        board[4][7].setPiece(bKing);
        board[5][7].setPiece(bBishop2);
        board[6][7].setPiece(bKnight2);
        board[7][7].setPiece(bRook2);
        board[0][6].setPiece(bPawn1);
        board[1][6].setPiece(bPawn2);
        board[2][6].setPiece(bPawn3);
        board[3][6].setPiece(bPawn4);
        board[4][6].setPiece(bPawn5);
        board[5][6].setPiece(bPawn6);
        board[6][6].setPiece(bPawn7);
        board[7][6].setPiece(bPawn8);

        //for test
        //board[2][2].setPiece(dummyPawn);

        printBoard(board);
        //GAMEPLAY
        bool checkMate = false;
        int counter = 0;
        int wKingX = 4;
        int bKingX = 4;
        int wKingY = 0;
        int bKingY = 7;
        while(!checkMate && counter < 10){
                if(counter%2 ==0){
                        //white to move
                        cout << "White, make a move: "<<endl;
                        bool made = false;
                        while(!made){
                                string move;
                                cin >> move;
                                //at the moment, a move while be a four character string, the initial position followed by the final position
                                vector<int> moven(4);
                                moven = algebraicConv(move, "white", board);
                                int toX, toY, fromX, fromY;
                                fromX = moven[0];
                                fromY = moven[1];
                                toX = moven[2];
                                toY = moven[3];
                                makeMove(board, fromX, fromY, toX, toY, "white", &wKingX, &wKingY, made);
                        }
                        printBoard(board);
                }else{
                        //black to move
                        cout << "Black, make a move: "<<endl;
                        bool made = false;
                        while(!made){
                                string move;
                                cin >> move;
                                //at the moment, a move will be a four character string, the initial position followed by the final position
                                vector<int> moven(4);
                                moven = algebraicConv(move, "black", board);
                                int toX, toY, fromX, fromY;
                                fromX = moven[0];
                                fromY = moven[1];
                                toX = moven[2];
                                toY = moven[3];
                                cout<< fromX<< " "<<fromY<<" "<<toX<<" "<<toY<<endl;
                                makeMove(board, fromX, fromY, toX, toY, "black", &bKingX, &bKingY, made);
                        }
                        printBoard(board);
                }
                counter++;
        } // end game
        return 0;
}

vector<int> getCos(string pos){
        vector<int> coords(4);
        coords[0] = pos[0] - 97;
        coords[1] = pos[1] - 49;
        coords[2] = pos[2] - 97;
        coords[3] = pos[3] - 49;
        return coords;
}

void printBoard(Square board[][8]){
        //print the piece identifiers in the terminal
        for(int i = 7; i >= 0; i--){
                cout<< i + 1 << "  ";
                for(int j = 0; j < 8; j++){
                        cout<<board[j][i].getPiece().getIdentifier()<<"   ";
                }
                cout<<endl;
                cout<<endl;
        }
        cout<< "    a     b     c     d     e     f     g     h";
        cout<<endl;
}

void makeMove(Square board[][8], int fromX, int fromY, int toX, int toY, string colour, int *KingX, int *KingY, bool &made){
        if(fromX < 0){
                cout<<"Invalid move. Make another: ";
                return;
        }
        Piece pieceToMove = board[fromX][fromY].getPiece();
        bool valid = true;
        if(colour == "white"){
                if(!pieceToMove.whiteCheckValid(fromX, fromY, toX, toY, board)){
                        valid = false;
                }
        }else if(colour == "black"){
                if(!pieceToMove.blackCheckValid(fromX, fromY, toX, toY, board)){
                        valid = false;
                }
        }else{
                valid = false;
        }
        if(valid){
                bool kingMove = false;
                if(fromX == *KingX && fromY == *KingY){
                        *KingX = toX;
                        *KingY = toY;
                        kingMove = true;
                }
                Piece tmp = board[toX][toY].getPiece();
                board[toX][toY].setPiece(board[fromX][fromY].getPiece());
                board[fromX][fromY].removePiece();
                if(colour == "white"){
                        if(isAttackedByBlack(*KingX, *KingY, board)){
                                board[fromX][fromY].setPiece(board[toX][toY].getPiece());
                                board[toX][toY].setPiece(tmp);
                                if(kingMove){
                                        *KingX = fromX;
                                        *KingY = fromY;
                                }
                                valid = false;
                        }
                }else if(colour == "black"){
                        if(isAttackedByWhite(*KingX, *KingY, board)){
                                board[fromX][fromY].setPiece(board[toX][toY].getPiece());
                                board[toX][toY].setPiece(tmp);
                                if(kingMove){
                                        *KingX = fromX;
                                        *KingY = fromY;
                                }
                                valid = false;
                        }
                }
        }

        if(valid){
                made = true;
        }else{
                cout<<"Invalid move. Make another: "<<endl;
        }
}

vector<int> algebraicConv(string move, string colour, Square board[][8]){
        vector<int> ret(4);
        if(move.size() == 2){
                // we are moving a pawn
                int file = move[0] - 97;
                int rank = move[1] - 49;
                if(colour == "white"){
                        if(board[file][rank - 1].getPiece().getIdentifier() == "wpa"){
                                ret[0] = file;
                                ret[1] = rank - 1;
                                ret[2] = file;
                                ret[3] = rank;
                                return ret;
                        }else if(board[file][rank - 2].getPiece().getIdentifier() == "wpa"){
                                ret[0] = file;
                                ret[1] = rank - 2;
                                ret[2] = file;
                                ret[3] = rank;
                                return ret;
                        }
                }else{
                        if(board[file][rank + 1].getPiece().getIdentifier() == "bpa"){
                                ret[0] = file;
                                ret[1] = rank + 1;
                                ret[2] = file;
                                ret[3] = rank;
                                return ret;
                        }else if(board[file][rank + 2].getPiece().getIdentifier() == "bpa"){
                                ret[0] = file;
                                ret[1] = rank + 2;
                                ret[2] = file;
                                ret[3] = rank;
                                return ret;
                        }else{
                                ret[0] = -1;
                                ret[1] = -1;
                                ret[2] = -1;
                                ret[3] = -1;
                                return ret;
                        }
                }
        }//finished considering move size 2;
        if(move.size() == 3){
                int file = move[1] - 97;
                int rank = move[2] - 49;
                ret[2] = file;
                ret[3] = rank;
                if(move[0] == 'N'){
                        if(colour == "white"){
                                if(rank - 2 >= 0){
                                        if(file - 1 >= 0){
                                                if(board[file-1][rank-2].getPiece().getIdentifier() == "wkn"){
                                                        ret[0] = file - 1;
                                                        ret[1] = rank - 2;
                                                        return ret;
                                                }
                                        }
                                        if(file + 1 < 8){
                                                cout<<"if(file + 1 < 8){"<<endl;
                                                if(board[file+1][rank-2].getPiece().getIdentifier() == "wkn"){
                                                        ret[0] = file + 1;
                                                        ret[1] = rank - 2;
                                                        return ret;
                                                }
                                        }
                                }
                                if(rank + 2 < 8){
                                        if(file - 1 >=0){
                                                if(board[file-1][rank+2].getPiece().getIdentifier() == "wkn"){
                                                        ret[0] = file - 1;
                                                        ret[1] = rank + 2;
                                                        return ret;
                                                }
                                        }
                                        if(file + 1 < 8){
                                                if(board[file+1][rank+2].getPiece().getIdentifier() == "wkn"){
                                                        ret[0] = file + 1;
                                                        ret[1] = rank + 2;
                                                        return ret;
                                                }
                                        }
                                }
                                if(file - 2 >= 0){
                                        if(rank - 1 >= 0){
                                                if(board[file-2][rank-1].getPiece().getIdentifier() == "wkn"){
                                                        ret[0] = file - 2;
                                                        ret[1] = rank - 1;
                                                        return ret;
                                                }
                                        }
                                        if(rank + 1 < 8){
                                                if(board[file-2][rank+1].getPiece().getIdentifier() == "wkn"){
                                                        ret[0] = file - 2;
                                                        ret[1] = rank + 1;
                                                        return ret;
                                                }
                                        }
                                }
                                if(file + 2 < 8){
                                        if(rank - 1 >= 0){
                                                if(board[file+2][rank-1].getPiece().getIdentifier() == "wkn"){
                                                        ret[0] = file + 2;
                                                        ret[1] = rank - 1;
                                                        return ret;
                                                }
                                        }
                                        if(rank + 1 < 8){
                                                if(board[file+2][rank+1].getPiece().getIdentifier() == "wkn"){
                                                        ret[0] = file -+2;
                                                        ret[1] = rank + 1;
                                                        return ret;
                                                }
                                        }
                                }
                                ret[0] = -1;
                                ret[1] = -1;
                                return ret;
                        }else{
                                if(rank - 2 >= 0){
                                        if(file - 1 >= 0){
                                                if(board[file-1][rank-2].getPiece().getIdentifier() == "bkn"){
                                                        ret[0] = file - 1;
                                                        ret[1] = rank - 2;
                                                        return ret;
                                                }
                                        }
                                        if(file + 1 < 8){
                                                if(board[file+1][rank-2].getPiece().getIdentifier() == "bkn"){
                                                        ret[0] = file + 1;
                                                        ret[1] = rank - 2;
                                                        return ret;
                                                }
                                        }
                                }
                                if(rank + 2 < 8){
                                        if(file - 1 >=0){
                                                if(board[file-1][rank+2].getPiece().getIdentifier() == "bkn"){
                                                        ret[0] = file - 1;
                                                        ret[1] = rank + 2;
                                                        return ret;
                                                }
                                        }
                                        if(file + 1 < 8){
                                                if(board[file+1][rank+2].getPiece().getIdentifier() == "bkn"){
                                                        ret[0] = file + 1;
                                                        ret[1] = rank + 2;
                                                        return ret;
                                                }
                                        }
                                }
                                if(file - 2 >= 0){
                                        if(rank - 1 >= 0){
                                                if(board[file-2][rank-1].getPiece().getIdentifier() == "bkn"){
                                                        ret[0] = file - 2;
                                                        ret[1] = rank - 1;
                                                        return ret;
                                                }
                                        }
                                        if(rank + 1 < 8){
                                                if(board[file-2][rank+1].getPiece().getIdentifier() == "bkn"){
                                                        ret[0] = file - 2;
                                                        ret[1] = rank + 1;
                                                        return ret;
                                                }
                                        }
                                }
                                if(file + 2 < 8){
                                        if(rank - 1 >= 0){
                                                if(board[file+2][rank-1].getPiece().getIdentifier() == "bkn"){
                                                        ret[0] = file + 2;
                                                        ret[1] = rank - 1;
                                                        return ret;
                                                }
                                        }
                                        if(rank + 1 < 8){
                                                if(board[file+2][rank+1].getPiece().getIdentifier() == "bkn"){
                                                        ret[0] = file -+2;
                                                        ret[1] = rank + 1;
                                                        return ret;
                                                }
                                        }
                                }
                                ret[0] = -1;
                                ret[1] = -1;
                                return ret;
                        }
                }else if(move[0] == 'B'){
                        if(colour == "white"){
                                for(int i = 1; file - i >= 0 && rank - i >= 0; i++){
                                        if(board[file - i][rank - i].getPiece().getIdentifier() == "wbi"){
                                                ret[0] = file - i;
                                                ret[1] = rank - i;
                                                return ret;
                                        }
                                }
                                for(int i = 1; file + i < 8 && rank - i >= 0; i++){
                                        if(board[file + i][rank - i].getPiece().getIdentifier() == "wbi"){
                                                ret[0] = file + i;
                                                ret[1] = rank - i;
                                                return ret;
                                        }
                                }
                                for(int i = 1; file - i >= 0 && rank + i < 8; i++){
                                        if(board[file - i][rank + i].getPiece().getIdentifier() == "wbi"){
                                                ret[0] = file - i;
                                                ret[1] = rank + i;
                                                return ret;
                                        }
                                }
                                for(int i = 1; file + i < 8 && rank + i < 8; i++){
                                        if(board[file + i][rank + i].getPiece().getIdentifier() == "wbi"){
                                                ret[0] = file + i;
                                                ret[1] = rank + i;
                                                return ret;
                                        }
                                }
                                ret[0] = -1;
                                ret[1] = -1;
                                return ret;
                        }else{
                                for(int i = 1; file - i >= 0 && rank - i >= 0; i++){
                                        if(board[file - i][rank - i].getPiece().getIdentifier() == "bbi"){
                                                ret[0] = file - i;
                                                ret[1] = rank - i;
                                                return ret;
                                        }
                                }
                                for(int i = 1; file + i < 8 && rank - i >= 0; i++){
                                        if(board[file + i][rank - i].getPiece().getIdentifier() == "bbi"){
                                                ret[0] = file + i;
                                                ret[1] = rank - i;
                                                return ret;
                                        }
                                }
                                for(int i = 1; file - i >= 0 && rank + i < 8; i++){
                                        if(board[file - i][rank + i].getPiece().getIdentifier() == "bbi"){
                                                ret[0] = file - i;
                                                ret[1] = rank + i;
                                                return ret;
                                        }
                                }
                                for(int i = 1; file + i < 8 && rank + i < 8; i++){
                                        if(board[file + i][rank + i].getPiece().getIdentifier() == "bbi"){
                                                ret[0] = file + i;
                                                ret[1] = rank + i;
                                                return ret;
                                        }
                                }
                                ret[0] = -1;
                                ret[1] = -1;
                                return ret;
                        }
                }else if(move[0] == 'R'){
                        if(colour == "white"){
                                for(int i = 1; file - i >= 0; i++){
                                        if(board[file - i][rank].getPiece().getIdentifier() == "wro"){
                                                ret[0] = file - i;
                                                ret[1] = rank;
                                                return ret;
                                        }
                                }
                                for(int i = 1; file + i < 8; i++){
                                        if(board[file + i][rank].getPiece().getIdentifier() == "wro"){
                                                ret[0] = file + i;
                                                ret[1] = rank;
                                                return ret;
                                        }
                                }
                                for(int i = 1; rank - i >= 0; i++){
                                        if(board[file][rank - i].getPiece().getIdentifier() == "wro"){
                                                ret[0] = file;
                                                ret[1] = rank - i;
                                                return ret;
                                        }
                                }
                                for(int i = 1; rank + i < 8; i++){
                                        if(board[file][rank + i].getPiece().getIdentifier() == "wro"){
                                                ret[0] = file;
                                                ret[1] = rank + i;
                                                return ret;
                                        }
                                }
                                //not found
                                ret[0] = -1;
                                ret[1] = -1;
                                return ret;
                        }else{
                                for(int i = 1; file - i >= 0; i++){
                                        if(board[file - i][rank].getPiece().getIdentifier() == "bro"){
                                                ret[0] = file - i;
                                                ret[1] = rank;
                                                return ret;
                                        }
                                }
                                for(int i = 1; file + i < 8; i++){
                                        if(board[file + i][rank].getPiece().getIdentifier() == "bro"){
                                                ret[0] = file + i;
                                                ret[1] = rank;
                                                return ret;
                                        }
                                }
                                for(int i = 1; rank - i >= 0; i++){
                                        if(board[file][rank - i].getPiece().getIdentifier() == "bro"){
                                                ret[0] = file;
                                                ret[1] = rank - i;
                                                return ret;
                                        }
                                }
                                for(int i = 1; rank + i < 8; i++){
                                        if(board[file][rank + i].getPiece().getIdentifier() == "bro"){
                                                ret[0] = file;
                                                ret[1] = rank + i;
                                                return ret;
                                        }
                                }
                                //not found
                                ret[0] = -1;
                                ret[1] = -1;
                                return ret;
                        }
                }else if(move[0] == 'Q'){
                        if(colour == "white"){
                                for(int i = 1; file - i >= 0; i++){
                                        if(board[file - i][rank].getPiece().getIdentifier() == "wqu"){
                                                ret[0] = file - i;
                                                ret[1] = rank;
                                                return ret;
                                        }
                                }
                                for(int i = 1; file + i < 8; i++){
                                        if(board[file + i][rank].getPiece().getIdentifier() == "wqu"){
                                                ret[0] = file + i;
                                                ret[1] = rank;
                                                return ret;
                                        }
                                }
                                for(int i = 1; rank - i >= 0; i++){
                                        if(board[file][rank - i].getPiece().getIdentifier() == "wqu"){
                                                ret[0] = file;
                                                ret[1] = rank - i;
                                                return ret;
                                        }
                                }
                                for(int i = 1; rank + i < 8; i++){
                                        if(board[file][rank + i].getPiece().getIdentifier() == "wqu"){
                                                ret[0] = file;
                                                ret[1] = rank + i;
                                                return ret;
                                        }
                                }
                                for(int i = 1; file - i >= 0 && rank - i >= 0; i++){
                                        if(board[file - i][rank - i].getPiece().getIdentifier() == "wqu"){
                                                ret[0] = file - i;
                                                ret[1] = rank - i;
                                                return ret;
                                        }
                                }
                                for(int i = 1; file + i < 8 && rank - i >= 0; i++){
                                        if(board[file + i][rank - i].getPiece().getIdentifier() == "wqu"){
                                                ret[0] = file + i;
                                                ret[1] = rank - i;
                                                return ret;
                                        }
                                }
                                for(int i = 1; file - i >= 0 && rank + i < 8; i++){
                                        if(board[file - i][rank + i].getPiece().getIdentifier() == "wqu"){
                                                ret[0] = file - i;
                                                ret[1] = rank + i;
                                                return ret;
                                        }
                                }
                                for(int i = 1; file + i < 8 && rank + i < 8; i++){
                                        if(board[file + i][rank + i].getPiece().getIdentifier() == "wqu"){
                                                ret[0] = file + i;
                                                ret[1] = rank + i;
                                                return ret;
                                        }
                                }
                                //not found
                                ret[0] = -1;
                                ret[1] = -1;
                                return ret;
                        }else{
                                for(int i = 1; file - i >= 0; i++){
                                        if(board[file - i][rank].getPiece().getIdentifier() == "bqu"){
                                                ret[0] = file - i;
                                                ret[1] = rank;
                                                return ret;
                                        }
                                }
                                for(int i = 1; file + i < 8; i++){
                                        if(board[file + i][rank].getPiece().getIdentifier() == "bqu"){
                                                ret[0] = file + i;
                                                ret[1] = rank;
                                                return ret;
                                        }
                                }
                                for(int i = 1; rank - i >= 0; i++){
                                        if(board[file][rank - i].getPiece().getIdentifier() == "bqu"){
                                                ret[0] = file;
                                                ret[1] = rank - i;
                                                return ret;
                                        }
                                }
                                for(int i = 1; rank + i < 8; i++){
                                        if(board[file][rank + i].getPiece().getIdentifier() == "bqu"){
                                                ret[0] = file;
                                                ret[1] = rank + i;
                                                return ret;
                                        }
                                }
                                for(int i = 1; file - i >= 0 && rank - i >= 0; i++){
                                        if(board[file - i][rank - i].getPiece().getIdentifier() == "bqu"){
                                                ret[0] = file - i;
                                                ret[1] = rank - i;
                                                return ret;
                                        }
                                }
                                for(int i = 1; file + i < 8 && rank - i >= 0; i++){
                                        if(board[file + i][rank - i].getPiece().getIdentifier() == "bqu"){
                                                ret[0] = file + i;
                                                ret[1] = rank - i;
                                                return ret;
                                        }
                                }
                                for(int i = 1; file - i >= 0 && rank + i < 8; i++){
                                        if(board[file - i][rank + i].getPiece().getIdentifier() == "bqu"){
                                                ret[0] = file - i;
                                                ret[1] = rank + i;
                                                return ret;
                                        }
                                }
                                for(int i = 1; file + i < 8 && rank + i < 8; i++){
                                        if(board[file + i][rank + i].getPiece().getIdentifier() == "bqu"){
                                                ret[0] = file + i;
                                                ret[1] = rank + i;
                                                return ret;
                                        }
                                }
                                //not found
                                ret[0] = -1;
                                ret[1] = -1;
                                return ret;
                        }
                }else if(move[0] == 'K'){
                        if(colour == "white"){
                                if(file + 1 < 8){
                                        if(rank + 1 < 8){
                                                if(board[file + 1][rank + 1].getPiece().getIdentifier() == "wki"){
                                                        ret[0] = file + 1;
                                                        ret[1] = rank + 1;
                                                        return ret;
                                                }
                                        }
                                        if(board[file + 1][rank].getPiece().getIdentifier() == "wki"){
                                                ret[0] = file + 1;
                                                ret[1] = rank;
                                                return ret;
                                        }
                                        if(rank - 1 >= 0){
                                                if(board[file + 1][rank - 1].getPiece().getIdentifier() == "wki"){
                                                        ret[0] = file + 1;
                                                        ret[1] = rank - 1;
                                                        return ret;
                                                }
                                        }
                                }
                                if(file - 1 >= 8){
                                        if(rank + 1 < 8){
                                                if(board[file - 1][rank + 1].getPiece().getIdentifier() == "wki"){
                                                        ret[0] = file - 1;
                                                        ret[1] = rank + 1;
                                                        return ret;
                                                }
                                        }
                                        if(board[file - 1][rank].getPiece().getIdentifier() == "wki"){
                                                ret[0] = file - 1;
                                                ret[1] = rank;
                                                return ret;
                                        }
                                        if(rank - 1 >= 0){
                                                if(board[file - 1][rank - 1].getPiece().getIdentifier() == "wki"){
                                                        ret[0] = file - 1;
                                                        ret[1] = rank - 1;
                                                        return ret;
                                                }
                                        }
                                }
                                if(rank + 1 < 8){
                                        if(board[file][rank+1].getPiece().getIdentifier() == "wki"){
                                                ret[0] = file;
                                                ret[1] = rank+1;
                                                return ret;
                                        }
                                }
                                if(rank - 1 >= 0){
                                        if(board[file][rank-1].getPiece().getIdentifier() == "wki"){
                                                ret[0] = file;
                                                ret[1] = rank-1;
                                                return ret;
                                        }
                                }
                                ret[0] = -1;
                                ret[1] = -1;
                                return ret;
                        }else{
                                if(file + 1 < 8){
                                        if(rank + 1 < 8){
                                                if(board[file + 1][rank + 1].getPiece().getIdentifier() == "bki"){
                                                        ret[0] = file + 1;
                                                        ret[1] = rank + 1;
                                                        return ret;
                                                }
                                        }
                                        if(board[file + 1][rank].getPiece().getIdentifier() == "bki"){
                                                ret[0] = file + 1;
                                                ret[1] = rank;
                                                return ret;
                                        }
                                        if(rank - 1 >= 0){
                                                if(board[file + 1][rank - 1].getPiece().getIdentifier() == "bki"){
                                                        ret[0] = file + 1;
                                                        ret[1] = rank - 1;
                                                        return ret;
                                                }
                                        }
                                }
                                if(file - 1 >= 8){
                                        if(rank + 1 < 8){
                                                if(board[file - 1][rank + 1].getPiece().getIdentifier() == "bki"){
                                                        ret[0] = file - 1;
                                                        ret[1] = rank + 1;
                                                        return ret;
                                                }
                                        }
                                        if(board[file - 1][rank].getPiece().getIdentifier() == "bki"){
                                                ret[0] = file - 1;
                                                ret[1] = rank;
                                                return ret;
                                        }
                                        if(rank - 1 >= 0){
                                                if(board[file - 1][rank - 1].getPiece().getIdentifier() == "bki"){
                                                        ret[0] = file - 1;
                                                        ret[1] = rank - 1;
                                                        return ret;
                                                }
                                        }
                                }
                                if(rank + 1 < 8){
                                        if(board[file][rank+1].getPiece().getIdentifier() == "bki"){
                                                ret[0] = file;
                                                ret[1] = rank+1;
                                                return ret;
                                        }
                                }
                                if(rank - 1 >= 0){
                                        if(board[file][rank-1].getPiece().getIdentifier() == "bki"){
                                                ret[0] = file;
                                                ret[1] = rank-1;
                                                return ret;
                                        }
                                }
                                ret[0] = -1;
                                ret[1] = -1;
                                return ret;
                        }//black
                }//end king
        }//end move.size() == 3
        if(move.size() == 4){
                int file = move[2] - 97;
                int rank = move[3] - 49;
                ret[2] = file;
                ret[3] = rank;
                if(move[1] == 'x'){
                        if(move[0] == 'N'){
                                if(colour == "white"){
                                        if(rank - 2 >= 0){
                                                if(file - 1 >= 0){
                                                        if(board[file-1][rank-2].getPiece().getIdentifier() == "wkn"){
                                                                ret[0] = file - 1;
                                                                ret[1] = rank - 2;
                                                                return ret;
                                                        }
                                                }
                                                if(file + 1 < 8){
                                                        if(board[file+1][rank-2].getPiece().getIdentifier() == "wkn"){
                                                                ret[0] = file + 1;
                                                                ret[1] = rank - 2;
                                                                return ret;
                                                        }
                                                }
                                        }
                                        if(rank + 2 < 8){
                                                if(file - 1 >=0){
                                                        if(board[file-1][rank+2].getPiece().getIdentifier() == "wkn"){
                                                                ret[0] = file - 1;
                                                                ret[1] = rank + 2;
                                                                return ret;
                                                        }
                                                }
                                                if(file + 1 < 8){
                                                        if(board[file+1][rank+2].getPiece().getIdentifier() == "wkn"){
                                                                ret[0] = file + 1;
                                                                ret[1] = rank + 2;
                                                                return ret;
                                                        }
                                                }
                                        }
                                        if(file - 2 >= 0){
                                                if(rank - 1 >= 0){
                                                        if(board[file-2][rank-1].getPiece().getIdentifier() == "wkn"){
                                                                ret[0] = file - 2;
                                                                ret[1] = rank - 1;
                                                                return ret;
                                                        }
                                                }
                                                if(rank + 1 < 8){
                                                        if(board[file-2][rank+1].getPiece().getIdentifier() == "wkn"){
                                                                ret[0] = file - 2;
                                                                ret[1] = rank + 1;
                                                                return ret;
                                                        }
                                                }
                                        }
                                        if(file + 2 < 8){
                                                if(rank - 1 >= 0){
                                                        if(board[file+2][rank-1].getPiece().getIdentifier() == "wkn"){
                                                                ret[0] = file + 2;
                                                                ret[1] = rank - 1;
                                                                return ret;
                                                        }
                                                }
                                                if(rank + 1 < 8){
                                                        if(board[file+2][rank+1].getPiece().getIdentifier() == "wkn"){
                                                                ret[0] = file + 2;
                                                                ret[1] = rank + 1;
                                                                return ret;
                                                        }
                                                }
                                        }
                                        ret[0] = -1;
                                        ret[1] = -1;
                                        return ret;
                                }else{
                                        if(rank - 2 >= 0){
                                                if(file - 1 >= 0){
                                                        if(board[file-1][rank-2].getPiece().getIdentifier() == "bkn"){
                                                                ret[0] = file - 1;
                                                                ret[1] = rank - 2;
                                                                return ret;
                                                        }
                                                }
                                                if(file + 1 < 8){
                                                        if(board[file+1][rank-2].getPiece().getIdentifier() == "bkn"){
                                                                ret[0] = file + 1;
                                                                ret[1] = rank - 2;
                                                                return ret;
                                                        }
                                                }
                                        }
                                        if(rank + 2 < 8){
                                                if(file - 1 >=0){
                                                        if(board[file-1][rank+2].getPiece().getIdentifier() == "bkn"){
                                                                ret[0] = file - 1;
                                                                ret[1] = rank + 2;
                                                                return ret;
                                                        }
                                                }
                                                if(file + 1 < 8){
                                                        if(board[file+1][rank+2].getPiece().getIdentifier() == "bkn"){
                                                                ret[0] = file + 1;
                                                                ret[1] = rank + 2;
                                                                return ret;
                                                        }
                                                }
                                        }
                                        if(file - 2 >= 0){
                                                if(rank - 1 >= 0){
                                                        if(board[file-2][rank-1].getPiece().getIdentifier() == "bkn"){
                                                                ret[0] = file - 2;
                                                                ret[1] = rank - 1;
                                                                return ret;
                                                        }
                                                }
                                                if(rank + 1 < 8){
                                                        if(board[file-2][rank+1].getPiece().getIdentifier() == "bkn"){
                                                                ret[0] = file - 2;
                                                                ret[1] = rank + 1;
                                                                return ret;
                                                        }
                                                }
                                        }
                                        if(file + 2 < 8){
                                                if(rank - 1 >= 0){
                                                        if(board[file+2][rank-1].getPiece().getIdentifier() == "bkn"){
                                                                ret[0] = file + 2;
                                                                ret[1] = rank - 1;
                                                                return ret;
                                                        }
                                                }
                                                if(rank + 1 < 8){
                                                        if(board[file+2][rank+1].getPiece().getIdentifier() == "bkn"){
                                                                ret[0] = file -+2;
                                                                ret[1] = rank + 1;
                                                                return ret;
                                                        }
                                                }
                                        }
                                        ret[0] = -1;
                                        ret[1] = -1;
                                        return ret;
                                }
                        }else if(move[0] == 'B'){
                                if(colour == "white"){
                                        for(int i = 1; file - i >= 0 && rank - i >= 0; i++){
                                                if(board[file - i][rank - i].getPiece().getIdentifier() == "wbi"){
                                                        ret[0] = file - i;
                                                        ret[1] = rank - i;
                                                        return ret;
                                                }
                                        }
                                        for(int i = 1; file + i < 8 && rank - i >= 0; i++){
                                                if(board[file + i][rank - i].getPiece().getIdentifier() == "wbi"){
                                                        ret[0] = file + i;
                                                        ret[1] = rank - i;
                                                        return ret;
                                                }
                                        }
                                        for(int i = 1; file - i >= 0 && rank + i < 8; i++){
                                                if(board[file - i][rank + i].getPiece().getIdentifier() == "wbi"){
                                                        ret[0] = file - i;
                                                        ret[1] = rank + i;
                                                        return ret;
                                                }
                                        }
                                        for(int i = 1; file + i < 8 && rank + i < 8; i++){
                                                if(board[file + i][rank + i].getPiece().getIdentifier() == "wbi"){
                                                        ret[0] = file + i;
                                                        ret[1] = rank + i;
                                                        return ret;
                                                }
                                        }
                                        ret[0] = -1;
                                        ret[1] = -1;
                                        return ret;
                                }else{
                                        for(int i = 1; file - i >= 0 && rank - i >= 0; i++){
                                                if(board[file - i][rank - i].getPiece().getIdentifier() == "bbi"){
                                                        ret[0] = file - i;
                                                        ret[1] = rank - i;
                                                        return ret;
                                                }
                                        }
                                        for(int i = 1; file + i < 8 && rank - i >= 0; i++){
                                                if(board[file + i][rank - i].getPiece().getIdentifier() == "bbi"){
                                                        ret[0] = file + i;
                                                        ret[1] = rank - i;
                                                        return ret;
                                                }
                                        }
                                        for(int i = 1; file - i >= 0 && rank + i < 8; i++){
                                                if(board[file - i][rank + i].getPiece().getIdentifier() == "bbi"){
                                                        ret[0] = file - i;
                                                        ret[1] = rank + i;
                                                        return ret;
                                                }
                                        }
                                        for(int i = 1; file + i < 8 && rank + i < 8; i++){
                                                if(board[file + i][rank + i].getPiece().getIdentifier() == "bbi"){
                                                        ret[0] = file + i;
                                                        ret[1] = rank + i;
                                                        return ret;
                                                }
                                        }
                                        ret[0] = -1;
                                        ret[1] = -1;
                                        return ret;
                                }
                        }else if(move[0] == 'R'){
                                if(colour == "white"){
                                        for(int i = 1; file - i >= 0; i++){
                                                if(board[file - i][rank].getPiece().getIdentifier() == "wro"){
                                                        ret[0] = file - i;
                                                        ret[1] = rank;
                                                        return ret;
                                                }
                                        }
                                        for(int i = 1; file + i < 8; i++){
                                                if(board[file + i][rank].getPiece().getIdentifier() == "wro"){
                                                        ret[0] = file + i;
                                                        ret[1] = rank;
                                                        return ret;
                                                }
                                        }
                                        for(int i = 1; rank - i >= 0; i++){
                                                if(board[file][rank - i].getPiece().getIdentifier() == "wro"){
                                                        ret[0] = file;
                                                        ret[1] = rank - i;
                                                        return ret;
                                                }
                                        }
                                        for(int i = 1; rank + i < 8; i++){
                                                if(board[file][rank + i].getPiece().getIdentifier() == "wro"){
                                                        ret[0] = file;
                                                        ret[1] = rank + i;
                                                        return ret;
                                                }
                                        }
                                        //not found
                                        ret[0] = -1;
                                        ret[1] = -1;
                                        return ret;
                                }else{
                                        for(int i = 1; file - i >= 0; i++){
                                                if(board[file - i][rank].getPiece().getIdentifier() == "bro"){
                                                        ret[0] = file - i;
                                                        ret[1] = rank;
                                                        return ret;
                                                }
                                        }
                                        for(int i = 1; file + i < 8; i++){
                                                if(board[file + i][rank].getPiece().getIdentifier() == "bro"){
                                                        ret[0] = file + i;
                                                        ret[1] = rank;
                                                        return ret;
                                                }
                                        }
                                        for(int i = 1; rank - i >= 0; i++){
                                                if(board[file][rank - i].getPiece().getIdentifier() == "bro"){
                                                        ret[0] = file;
                                                        ret[1] = rank - i;
                                                        return ret;
                                                }
                                        }
                                        for(int i = 1; rank + i < 8; i++){
                                                if(board[file][rank + i].getPiece().getIdentifier() == "bro"){
                                                        ret[0] = file;
                                                        ret[1] = rank + i;
                                                        return ret;
                                                }
                                        }
                                        //not found
                                        ret[0] = -1;
                                        ret[1] = -1;
                                        return ret;
                                }
                        }else if(move[0] == 'Q'){
                                if(colour == "white"){
                                        for(int i = 1; file - i >= 0; i++){
                                                if(board[file - i][rank].getPiece().getIdentifier() == "wqu"){
                                                        ret[0] = file - i;
                                                        ret[1] = rank;
                                                        return ret;
                                                }
                                        }
                                        for(int i = 1; file + i < 8; i++){
                                                if(board[file + i][rank].getPiece().getIdentifier() == "wqu"){
                                                        ret[0] = file + i;
                                                        ret[1] = rank;
                                                        return ret;
                                                }
                                        }
                                        for(int i = 1; rank - i >= 0; i++){
                                                if(board[file][rank - i].getPiece().getIdentifier() == "wqu"){
                                                        ret[0] = file;
                                                        ret[1] = rank - i;
                                                        return ret;
                                                }
                                        }
                                        for(int i = 1; rank + i < 8; i++){
                                                if(board[file][rank + i].getPiece().getIdentifier() == "wqu"){
                                                        ret[0] = file;
                                                        ret[1] = rank + i;
                                                        return ret;
                                                }
                                        }
                                        for(int i = 1; file - i >= 0 && rank - i >= 0; i++){
                                                if(board[file - i][rank - i].getPiece().getIdentifier() == "wqu"){
                                                        ret[0] = file - i;
                                                        ret[1] = rank - i;
                                                        return ret;
                                                }
                                        }
                                        for(int i = 1; file + i < 8 && rank - i >= 0; i++){
                                                if(board[file + i][rank - i].getPiece().getIdentifier() == "wqu"){
                                                        ret[0] = file + i;
                                                        ret[1] = rank - i;
                                                        return ret;
                                                }
                                        }
                                        for(int i = 1; file - i >= 0 && rank + i < 8; i++){
                                                if(board[file - i][rank + i].getPiece().getIdentifier() == "wqu"){
                                                        ret[0] = file - i;
                                                        ret[1] = rank + i;
                                                        return ret;
                                                }
                                        }
                                        for(int i = 1; file + i < 8 && rank + i < 8; i++){
                                                if(board[file + i][rank + i].getPiece().getIdentifier() == "wqu"){
                                                        ret[0] = file + i;
                                                        ret[1] = rank + i;
                                                        return ret;
                                                }
                                        }
                                        //not found
                                        ret[0] = -1;
                                        ret[1] = -1;
                                        return ret;
                                }else{
                                        for(int i = 1; file - i >= 0; i++){
                                                if(board[file - i][rank].getPiece().getIdentifier() == "bqu"){
                                                        ret[0] = file - i;
                                                        ret[1] = rank;
                                                        return ret;
                                                }
                                        }
                                        for(int i = 1; file + i < 8; i++){
                                                if(board[file + i][rank].getPiece().getIdentifier() == "bqu"){
                                                        ret[0] = file + i;
                                                        ret[1] = rank;
                                                        return ret;
                                                }
                                        }
                                        for(int i = 1; rank - i >= 0; i++){
                                                if(board[file][rank - i].getPiece().getIdentifier() == "bqu"){
                                                        ret[0] = file;
                                                        ret[1] = rank - i;
                                                        return ret;
                                                }
                                        }
                                        for(int i = 1; rank + i < 8; i++){
                                                if(board[file][rank + i].getPiece().getIdentifier() == "bqu"){
                                                        ret[0] = file;
                                                        ret[1] = rank + i;
                                                        return ret;
                                                }
                                        }
                                        for(int i = 1; file - i >= 0 && rank - i >= 0; i++){
                                                if(board[file - i][rank - i].getPiece().getIdentifier() == "bqu"){
                                                        ret[0] = file - i;
                                                        ret[1] = rank - i;
                                                        return ret;
                                                }
                                        }
                                        for(int i = 1; file + i < 8 && rank - i >= 0; i++){
                                                if(board[file + i][rank - i].getPiece().getIdentifier() == "bqu"){
                                                        ret[0] = file + i;
                                                        ret[1] = rank - i;
                                                        return ret;
                                                }
                                        }
                                        for(int i = 1; file - i >= 0 && rank + i < 8; i++){
                                                if(board[file - i][rank + i].getPiece().getIdentifier() == "bqu"){
                                                        ret[0] = file - i;
                                                        ret[1] = rank + i;
                                                        return ret;
                                                }
                                        }
                                        for(int i = 1; file + i < 8 && rank + i < 8; i++){
                                                if(board[file + i][rank + i].getPiece().getIdentifier() == "bqu"){
                                                        ret[0] = file + i;
                                                        ret[1] = rank + i;
                                                        return ret;
                                                }
                                        }
                                        //not found
                                        ret[0] = -1;
                                        ret[1] = -1;
                                        return ret;
                                }
                        }else if(move[0] == 'K'){
                                if(colour == "white"){
                                        if(file + 1 < 8){
                                                if(rank + 1 < 8){
                                                        if(board[file + 1][rank + 1].getPiece().getIdentifier() == "wki"){
                                                                ret[0] = file + 1;
                                                                ret[1] = rank + 1;
                                                                return ret;
                                                        }
                                                }
                                                if(board[file + 1][rank].getPiece().getIdentifier() == "wki"){
                                                        ret[0] = file + 1;
                                                        ret[1] = rank;
                                                        return ret;
                                                }
                                                if(rank - 1 >= 0){
                                                        if(board[file + 1][rank - 1].getPiece().getIdentifier() == "wki"){
                                                                ret[0] = file + 1;
                                                                ret[1] = rank - 1;
                                                                return ret;
                                                        }
                                                }
                                        }
                                        if(file - 1 >= 8){
                                                if(rank + 1 < 8){
                                                        if(board[file - 1][rank + 1].getPiece().getIdentifier() == "wki"){
                                                                ret[0] = file - 1;
                                                                ret[1] = rank + 1;
                                                                return ret;
                                                        }
                                                }
                                                if(board[file - 1][rank].getPiece().getIdentifier() == "wki"){
                                                        ret[0] = file - 1;
                                                        ret[1] = rank;
                                                        return ret;
                                                }
                                                if(rank - 1 >= 0){
                                                        if(board[file - 1][rank - 1].getPiece().getIdentifier() == "wki"){
                                                                ret[0] = file - 1;
                                                                ret[1] = rank - 1;
                                                                return ret;
                                                        }
                                                }
                                        }
                                        if(rank + 1 < 8){
                                                if(board[file][rank+1].getPiece().getIdentifier() == "wki"){
                                                        ret[0] = file;
                                                        ret[1] = rank+1;
                                                        return ret;
                                                }
                                        }
                                        if(rank - 1 >= 0){
                                                if(board[file][rank-1].getPiece().getIdentifier() == "wki"){
                                                        ret[0] = file;
                                                        ret[1] = rank-1;
                                                        return ret;
                                                }
                                        }
                                        ret[0] = -1;
                                        ret[1] = -1;
                                        return ret;
                                }else{
                                        if(file + 1 < 8){
                                                if(rank + 1 < 8){
                                                        if(board[file + 1][rank + 1].getPiece().getIdentifier() == "bki"){
                                                                ret[0] = file + 1;
                                                                ret[1] = rank + 1;
                                                                return ret;
                                                        }
                                                }
                                                if(board[file + 1][rank].getPiece().getIdentifier() == "bki"){
                                                        ret[0] = file + 1;
                                                        ret[1] = rank;
                                                        return ret;
                                                }
                                                if(rank - 1 >= 0){
                                                        if(board[file + 1][rank - 1].getPiece().getIdentifier() == "bki"){
                                                                ret[0] = file + 1;
                                                                ret[1] = rank - 1;
                                                                return ret;
                                                        }
                                                }
                                        }
                                        if(file - 1 >= 8){
                                                if(rank + 1 < 8){
                                                        if(board[file - 1][rank + 1].getPiece().getIdentifier() == "bki"){
                                                                ret[0] = file - 1;
                                                                ret[1] = rank + 1;
                                                                return ret;
                                                        }
                                                }
                                                if(board[file - 1][rank].getPiece().getIdentifier() == "bki"){
                                                        ret[0] = file - 1;
                                                        ret[1] = rank;
                                                        return ret;
                                                }
                                                if(rank - 1 >= 0){
                                                        if(board[file - 1][rank - 1].getPiece().getIdentifier() == "bki"){
                                                                ret[0] = file - 1;
                                                                ret[1] = rank - 1;
                                                                return ret;
                                                        }
                                                }
                                        }
                                        if(rank + 1 < 8){
                                                if(board[file][rank+1].getPiece().getIdentifier() == "bki"){
                                                        ret[0] = file;
                                                        ret[1] = rank+1;
                                                        return ret;
                                                }
                                        }
                                        if(rank - 1 >= 0){
                                                if(board[file][rank-1].getPiece().getIdentifier() == "bki"){
                                                        ret[0] = file;
                                                        ret[1] = rank-1;
                                                        return ret;
                                                }
                                        }
                                        ret[0] = -1;
                                        ret[1] = -1;
                                        return ret;
                                }//black
                        }else if(move[0] == 'a' ||
                                move[0] == 'b' ||
                                move[0] == 'c' ||
                                move[0] == 'd' ||
                                move[0] == 'e' ||
                                move[0] == 'f' ||
                                move[0] == 'g' ||
                                move[0] == 'h'){
                                        //pawn capturing
                                                int fromFile = move[0] - 97;
                                                if(colour == "white"){
                                                        if(board[fromFile][rank - 1].getPiece().getIdentifier() == "wpa"){
                                                                ret[0] = fromFile;
                                                                ret[1] = rank - 1;
                                                                return ret;
                                                        }else{
                                                                ret[0] = -1;
                                                                ret[1] = -1;
                                                                return ret;
                                                        }
                                                }else{
                                                        if(board[fromFile][rank + 1].getPiece().getIdentifier() == "bpa"){
                                                                ret[0] = fromFile;
                                                                ret[1] = rank + 1;
                                                                return ret;
                                                        }
                                                }
                                        }
                        }//end capture

        }//end move.size() == 4



        ret[0] = -1;
        ret[1] = -1;
        ret[2] = -1;
        ret[3] = -1;
        return ret;
}//end function
/*
vector<int> getCos(string pos){
        vector<int> coords(4);
        coords[0] = pos[0] - 97;
        coords[1] = pos[1] - 49;
        coords[2] = pos[2] - 97;
        coords[3] = pos[3] - 49;
        return coords;
}
*/
