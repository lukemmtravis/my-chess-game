#include <string>

using namespace std;

class Square;

class Piece{
        protected:
                string colour;
                string type;
                bool first;
        public:
                Piece(){
                        colour = "clear";
                        type = "empty";
                        first = true;
                }
                Piece(string col, string t){
                        colour = col;
                        type = t;
                        first = true;
                }
                string getColour(){
                        return colour;
                }
                string getType(){
                        return type;
                }
                string getIdentifier(){
                        if(colour != "clear"){
                                return colour[0] + type.substr(0, 2);
                        }else{
                                return "...";
                        }
                }

                bool whiteCheckValid(int fromX, int fromY, int toX, int toY, Square board[][8]);

                bool blackCheckValid(int fromX, int fromY, int toX, int toY, Square board[][8]);

                bool firstMove(){
                        return first;
                }
                void notFirst(){
                        first = false;
                }
                // bool kingCheck(string move) = 0;

};

/*class Pawn: public Piece{

        public:
                Pawn(){
                        colour = "clear";
                        type = "pawn";
                        first = true;
                }
                Pawn(string col){
                        first = true;
                        colour = col;
                        type = "pawn";
                }
                //bool kingCheck(string move){}
                bool checkValid(int fromX, int fromY, int toX, int toY){
                        cout<<"pawn checkValid()"<<endl;
                        if((toY - fromY) == 1 && toX==fromX){
                                return true;
                        }else if((toY - fromY) == 2 && toX==fromX){
                                if(first){
                                        return true;
                                }else{
                                        return false;
                                }
                        }
                        return false;
                }
};
class Bishop: public Piece{
        public:
                Bishop(){
                        colour = "clear";
                        type = "bishop";
                        first = true;
                }
                Bishop(string col){
                        colour = col;
                        type = "bishop";
                        first = true;
                }
                bool checkValid(int fromX, int fromY, int toX, int toY){
                        return true;
                }
                //bool kingCheck(string move){}
};
class Knight: public Piece{
        public:
                Knight(){
                        colour = "clear";
                        type = "knight";
                        first = true;
                }
                Knight(string col){
                        colour = col;
                        type = "knight";
                        first = true;
                }
                bool checkValid(int fromX, int fromY, int toX, int toY){
                        return true;
                }
        //bool kingCheck(string move){}
};
class Rook: public Piece{
        public:
                Rook(){
                        colour = "clear";
                        type = "rook";
                }
                Rook(string col){
                        colour = col;
                        type = "rook";
                }
                bool checkValid(int fromX, int fromY, int toX, int toY){
                        return true;
                }
                //bool kingCheck(string move){}
};
class Queen: public Piece{
        public:
                Queen(){
                        colour = "clear";
                        type = "queen";
                }
                Queen(string col){
                        colour = col;
                        type = "queen";
                }
                bool checkValid(int fromX, int fromY, int toX, int toY){
                        return true;
                }
                //bool kingCheck(string move){}
};
class King: public Piece{
        protected:
                bool inCheck;
        public:
                King(){
                        colour = "clear";
                        inCheck = false;
                        type = "king";
                }
                King(string col){
                        colour = col;
                        inCheck = false;
                        type = "king";
                }
                bool checkValid(int fromX, int fromY, int toX, int toY){
                        return true;
                }
                bool Check(){
                        return inCheck; // at the moment for testing;
                }
};
class noPiece: public Piece{
        public:
                noPiece(){
                        colour = "clear";
                        type = "empty";
                }
                string getIdentifier(){
                        return "...";
                }
                bool checkValid(int fromX, int fromY, int toX, int toY){
                        return false;
                }
};
*/

class Square{
private:
        bool occupied;
public:
        Piece piece;
        Square(){
                occupied = false;
                Piece *p = new Piece();
                piece = *p;
        }
        Square(Piece p){
                if (p.getColour() != "clear"){
                        occupied = true;
                        piece = p;
                }else{
                        occupied = false;
                        piece = p;
                }
        }

        bool hasPiece(){
                return occupied;
        }
        
        void setPiece(Piece p){
                piece = p;
        }

        bool isOccupied(){
                return occupied;
        }

        Piece getPiece(){
                return piece;
        }

        void removePiece(){
                Piece *p = new Piece();
                piece = *p;
        }

};
